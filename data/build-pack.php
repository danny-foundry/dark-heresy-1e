<?php

$files = [
    'characteristics' => [
        'file' => __DIR__ . '/characteristics.csv',
        'targetFile' => dirname(__DIR__) . '/packs/characteristics.db',
        'handler' => 'importCharacteristic',
    ],
    'skills' => [
        'file' => __DIR__ . '/skills.csv',
        'targetFile' => dirname(__DIR__).'/packs/skills.db',
        'handler' => 'importSkill',
    ],
    'talents' => [
        'file' => __DIR__.'/talents.csv',
        'targetFile' => dirname(__DIR__).'/packs/talents.db',
        'handler' => 'importTalent',
    ],
    'weapons' => [
        'file' => __DIR__.'/weapons.csv',
        'targetFile' => dirname(__DIR__).'/packs/weapons.db',
        'handler' => 'importWeapon',
    ],
    'armour' => [
        'file' => __DIR__.'/armour.csv',
        'targetFile' => dirname(__DIR__).'/packs/armour.db',
        'handler' => 'importArmour',
    ]
];
$file = $argv[1] ?? '';
$target = $files[$file] ?? null;
if (!$target) {
    echo "\e[33mInvalid file\e[0m\n";
}

if (file_exists($target['targetFile'])) {
    unlink($target['targetFile']);
}

$id = random_int(10000, 90000);
$out = fopen($target['targetFile'], 'wb');
$handle = fopen($target['file'], 'r');

fgets($handle);
while ($row = fgetcsv($handle)) {
    $output = call_user_func($target['handler'], $row, base_convert($id, 10, 32));
    fwrite($out, json_encode($output)."\n");
    $id ++;
}

fclose($out);
fclose($handle);

function importCharacteristic($row, $id)
{
    $name = $row[0];
    $abbreviation = $row[1];
    $hasBonus = strtolower($row[2]) === 'true';
    $position = $row[3];

    return [
        '_id' => $id,
        'name' => $name,
        'permission' => [
            'default' => 0,
        ],
        'type' => 'characteristic',
        'flags' => (object)[],
        'img' => '',
        'effects' => [],
        'system' => [
            'abbreviation' => $abbreviation,
            'has_bonus' => $hasBonus,
            'base' => 20,
            'training' => 0,
            'position' => $position,
        ]
    ];
}


function importSkill($row, $id)
{
    $name = $row[0];
    $characteristic = $row[1];
    $isBasic = (bool)$row[2];

    return [
        '_id' => $id,
        'name' => $name,
        'permission' => [
            'default' => 0,
        ],
        'type' => 'skill',
        'flags' => (object)[],
        'img' => '',
        'effects' => [],
        'system' => [
            'is_basic' => $isBasic,
            'level' => 0,
            'characteristic' => $characteristic,
        ]
    ];
}

function importTalent($row, $id)
{
    $name = $row[0];
    $roll = $row[1];
    $level = (int)($row[2] ?? 0);
    $description = $row[3];
    $modifiers = $row[4];

    return [
        '_id' => $id,
        'name' => $name,
        'permission' => [
            'default' => 0,
        ],
        'type' => 'talent',
        'flags' => (object)[],
        'img' => '',
        'effects' => [],
        'system' => [
            'name' => $name,
            'level' => $level,
            'roll' => $roll,
            'description' => $description,
            'modifiers' => $modifiers,
        ]
    ];
}

function importWeapon($row, $id)
{
    $keywords = [
        "accurate"=> false,
        "balanced"=> false,
        "defensive"=> false,
        "flame"=> false,
        "flexible"=> false,
        "inaccurate"=> false,
        "overheats"=> false,
        "power_field"=> false,
        "primitive"=> false,
        "recharge"=> false,
        "reliable"=> false,
        "scatter"=> false,
        "shocking"=> false,
        "smoke"=> false,
        "snare"=> false,
        "tearing"=> false,
        "toxic"=> false,
        "unbalanced"=> false,
        "unreliable"=> false,
        "unstable"=> false,
        "unwieldy"=> false
    ];
    
    $name = $row[0];
    $attackType = $row[1]; // Melee/Ranged/Thrown
    $subType = $row[2]; // las etc
    $class = $row[3]; // Basic/Pistol etc
    $range = $row[4];
    $rateOfFire = $row[5];
    $noDice = $row[6];
    $diceSize = $row[7];
    $diceMod = $row[8];
    $damageType = $row[9];
    $penetration = $row[10];
    $clip = $row[11];
    $reload = $row[12];
    $blast = $row[13];
    $assignedKeywords = array_map(
        fn($word) => strtr(strtolower(trim($word)), [' ' => '_']),
        explode(',', $row[14])
    );

    if ($diceMod >= 0) {
        $diceMod = '+'.$diceMod;
    }

    $type = $range ? 'ranged_weapon' : 'melee_weapon';
    $characteristic = $attackType === 'Melee' ? 'WS' : 'BS';
    $talent = $class.' Weapon Training ('.$subType.')';
    $damage = $noDice.'d'.$diceSize.$diceMod;
    foreach ($assignedKeywords as $keyword) {
        $keywords[$keyword] = true;
    }

    return [
        '_id' => $id,
        'name' => $name,
        'permission' => [
            'default' => 0,
        ],
        'type' => $type,
        'flags' => (object)[],
        'img' => '',
        'effects' => [],
        'system' => [
            'class' => $class,
            'talent' => $talent,
            'attack_type' => $attackType,
            'characteristic' => $characteristic,
            'damage' => $damage,
            'type' => $damageType,
            'penetration' => $penetration,
            'blast' => $blast,
            'range' => $range,
            'rate_of_fire' => $rateOfFire,
            'clip' => $clip,
            'reload' => $reload,
            'keywords' => $keywords,
        ]
    ];
}

function importArmour($row, $id)
{
    $name = $row[0];
    $type = $row[1];
    $head = $row[2];
    $body = $row[3];
    $arms = $row[4];
    $legs = $row[5];

    $modifiers = '';
    if ($type === 'Power') {
        $modifiers = 'characteristic.S=20';
    }

    return [
        '_id' => $id,
        'name' => $name,
        'permission' => [
            'default' => 0,
        ],
        'type' => 'armour',
        'flags' => (object)[],
        'img' => '',
        'effects' => [],
        'system' => [
            'head' => $head,
            'body' => $body,
            'left_arm' => $arms,
            'right_arm' => $arms,
            'left_leg' => $legs,
            'right_leg' => $legs,
            'description' => '',
            'is_equipped' => true,
            'modifiers' => $modifiers,
        ]
    ];
}