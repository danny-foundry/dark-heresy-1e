import { DarkHeresyActor } from './classes/actor.mjs';
import { DarkHeresyItem } from './classes/item.mjs';
import { DarkHeresySkillSheet } from './sheets/skill.mjs';
import { DarkHeresyAcolyteSheet } from './sheets/acolyte.mjs';
import { DarkHeresyCharacteristicSheet } from './sheets/characteristic.mjs';
import { DarkHeresyTalentSheet } from "./sheets/talent.mjs";
import { DarkHeresyPsychicPowerSheet } from "./sheets/psychic-power.mjs";
import { DarkHeresyMeleeWeaponSheet } from "./sheets/melee-weapon.mjs";
import { DarkHeresyRangedWeaponSheet } from "./sheets/ranged-weapon.mjs";
import { DarkHeresyArmourSheet } from "./sheets/armour.mjs";
import { DarkHeresyOtherGearSheet } from "./sheets/other-gear.mjs";
import { DarkHeresyMalignancySheet } from "./sheets/malignancy.mjs";
import { DarkHeresyMutationSheet } from "./sheets/mutation.mjs";


Hooks.once('init', () => {
    try {
        CONFIG.Combat.initiative = {formula: "1d10 + @initiative", decimals: 0};
        CONFIG.Actor.documentClass = DarkHeresyActor;
        CONFIG.Item.documentClass = DarkHeresyItem;

        Actors.unregisterSheet("core", ActorSheet);
        Items.unregisterSheet("core", ItemSheet);

        Actors.registerSheet("dark-heresy-1e", DarkHeresyAcolyteSheet, { types: ["acolyte"], makeDefault: true });
        Items.registerSheet("dark-heresy-1e", DarkHeresySkillSheet, { types: ["skill"] });
        Items.registerSheet("dark-heresy-1e", DarkHeresyCharacteristicSheet, { types: ["characteristic"]} );
        Items.registerSheet("dark-heresy-1e", DarkHeresyTalentSheet, { types: ["talent"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyPsychicPowerSheet, { types: ["psychic_power"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyMeleeWeaponSheet, { types: ["melee_weapon"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyRangedWeaponSheet, { types: ["ranged_weapon"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyArmourSheet, { types: ["armour"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyOtherGearSheet, { types: ["other_gear"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyMutationSheet, { types: ["mutation"], makeDefault: true } );
        Items.registerSheet("dark-heresy-1e", DarkHeresyMalignancySheet, { types: ["malignancy"], makeDefault: true } );

        setupHandlebarsHelpers();
        setupHandlebarsTemplates();

    } catch (err) {
        console.error(err);
    }
});

function setupHandlebarsHelpers()
{
    Handlebars.registerHelper("nl2br", function(text) {
        text = Handlebars.Utils.escapeExpression(text);
        text = text.replace(/(\r\n|\n|\r)/gm, '<br>');

        return new Handlebars.SafeString(text);
    });
}

function setupHandlebarsTemplates()
{
    const templatePaths = [
        "systems/dark-heresy-1e/template/chat/partials/attack-notices.hbs",
        "systems/dark-heresy-1e/template/chat/partials/attack-summary.hbs",

        "systems/dark-heresy-1e/template/sheet/actor/acolyte-sheet.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/limited-sheet.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/all-skills.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/armour.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/advanced-skills.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/basic-skills.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/characteristics.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/disorders.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/items.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/malignancies.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/mutations.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/psychic-powers.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/talents.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/weapons.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/weapon-for-combat.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/partials/skill.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/combat.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/description.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/gear.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/stats.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/talents.hbs",
        "systems/dark-heresy-1e/template/sheet/actor/tabs/debug.hbs",
        "systems/dark-heresy-1e/template/sheet/item/partials/gear-shared.hbs",
        "systems/dark-heresy-1e/template/sheet/item/partials/weapons-shared.hbs",
        "systems/dark-heresy-1e/template/sheet/item/characteristic-sheet.hbs",
        "systems/dark-heresy-1e/template/sheet/item/limited-sheet.hbs",
        "systems/dark-heresy-1e/template/sheet/item/skill-sheet.hbs",
        "systems/dark-heresy-1e/template/sheet/item/talent-sheet.hbs",

    ];
    return loadTemplates(templatePaths);
}