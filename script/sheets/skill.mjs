
export class DarkHeresySkillSheet extends ItemSheet
{
    activateListeners(html) {
        super.activateListeners(html);
    }

    /** @override */
    async getData() {
        const data = super.getData();

        data.isEmbedded = data.item.isEmbedded;
        if (data.isEmbedded) {
            data.characteristics = data.item.actor.characteristicOptions;
        } else {
            data.characteristics = {};

            let items = [...Items.instance].sort((a, b) => a.system.position - b.system.position)
            for (let item of items) {
                if (item.type !== 'characteristic') {
                    continue;
                }
                data.characteristics[item.system.abbreviation] = item.name;
            }
        }

        return data;
    }

    /** @override */
    get template() {
        if (!game.user.isGM && this.actor.limited) {
            return "systems/dark-heresy-1e/template/sheet/item/limited-sheet.hbs";
        } else {
            return "systems/dark-heresy-1e/template/sheet/item/skill-sheet.hbs";
        }
    }
}