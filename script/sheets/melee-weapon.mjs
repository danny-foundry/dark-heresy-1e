
export class DarkHeresyMeleeWeaponSheet extends ItemSheet
{
    activateListeners(html) {
        super.activateListeners(html);
    }

    /** @override */
    async getData() {
        const data = super.getData();

        data.isEmbedded = data.item.isEmbedded;
        data.isGM = game.user.isGM;
        data.talents = data.item.availableTalentNames;
        if (data.isEmbedded) {
            data.characteristics = data.item.actor.characteristicOptions;
        } else {
            data.characteristics = {};

            let items = [...Items.instance].sort((a, b) => a.system.position - b.system.position)
            for (let item of items) {
                if (item.type !== 'characteristic') {
                    continue;
                }
                data.characteristics[item.system.abbreviation] = item.name;
            }
        }

        return data;
    }

    /** @override */
    get template() {
        if (!game.user.isGM && this.actor.limited) {
            return "systems/dark-heresy-1e/template/sheet/item/limited-sheet.hbs";
        } else {
            return "systems/dark-heresy-1e/template/sheet/item/melee-weapon-sheet.hbs";
        }
    }

    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        buttons = [
            {
                label: 'Chat',
                class: 'send-to-chat',
                icon: 'fas fa-comment',
                onclick: async (evt) => this.item.model.sendToChat(),
            },
            ...buttons,
        ];

        return buttons;
    }
}