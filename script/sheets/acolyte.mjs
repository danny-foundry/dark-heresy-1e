import {DiceRoll} from "../helpers/dice.mjs";
import {showAttackDialog, showDamageDialog, showPsychicPowerDialog, showTakeDamageDialog} from "../helpers/dialogs.js";
import {hitLocations, locationByNumber} from "../classes/ref/hit-locations.mjs";
import {sendToChat} from "../helpers/chat.mjs";
import {totalModifier} from "../helpers/actor.mjs";
import {AttackData, keywords, ParryData} from "../helpers/combat.mjs";

export class DarkHeresyAcolyteSheet extends ActorSheet
{
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["dark-heresy-1e", "sheet", "actor"],
            template: "systems/dark-heresy-1e/template/sheet/actor/acolyte.hbs",
            width: 900,
            height: 881,
            resizable: false,
            tabs: [
                {
                    navSelector: ".sheet-tabs",
                    contentSelector: ".sheet-body",
                    initial: "stats"
                }
            ]
        });
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('[data-action="edit"]').click((evt) => {
            let itemID = evt.currentTarget.getAttribute('data-item');
            let item = this.actor.items.get(itemID);

            item.sheet.render(true);
        });

        html.find('[data-action="reload"]').click(async (evt) => {
            let itemID = evt.currentTarget.getAttribute('data-weapon');
            let item = this.actor.items.get(itemID);

            if (!item.model.usesAmmo) {
                return;
            }

            await sendToChat("systems/dark-heresy-1e/template/chat/reload.hbs", { actor: this.actor, weapon: item });
            item.update({
                system: {
                    current_ammo: null,
                }
            })
        });

        html.find('[data-action="take-damage"]').click(async (evt) => {
            let locationName = evt.currentTarget.getAttribute('data-location');
            let location = hitLocations.find((location) => location.name === locationName);
            if (!location) {
                console.error("Failed to locate " + locationName);
                alert("Bad location");
                return;
            }

            await showTakeDamageDialog(this.actor, location);
        });

        html.find('[data-action="empty-clip"]').click(async (evt) => {
            let itemID = evt.currentTarget.getAttribute('data-weapon');
            let item = this.actor.items.get(itemID);

            if (!item.model.usesAmmo) {
                return;
            }
            await sendToChat("systems/dark-heresy-1e/template/chat/eject-clip.hbs", { actor: this.actor, weapon: item });
            item.update({
                system: {
                    current_ammo: 0,
                }
            })
        });

        html.find('[data-action="delete"]').click((evt) => {
            let itemID = evt.currentTarget.getAttribute('data-item');
            this.actor.deleteEmbeddedDocuments("Item", [itemID]);
        });

        html.find('[data-action="update"]').click((evt) => {
            this.actor.updateEmbeddedItems();
        });

        html.find('[data-roll]').click(async (evt) => {
            const pool = evt.currentTarget.getAttribute('data-roll');
            const roll = new DiceRoll(this.actor);

            try {
                await roll.parseAndRoll(pool);
            } catch (err) {
                if (typeof err === 'string') {
                    alert(err);
                }
                throw err;
            }

            //rollStats(this.actor, roll);
        });

        html.find('[data-action="roll-item"]').click(async (evt) => {
            let itemID = evt.currentTarget.getAttribute('data-item');
            let item = this.actor.items.get(itemID);

            console.log(this.actor.characteristics);
            if (item.type === 'psychic_power') {
                await showPsychicPowerDialog(this.actor, item);
            }
        });

        html.find('[data-action="roll-initiative"]').click(async (evt) => {
            let modifier = totalModifier(this.actor, 'initiative');
            let base = this.actor.characteristics.Ag.bonus;

            const roll = new Roll(`1d10+${base}+${modifier}`);

            try {
                await roll.evaluate({ async: true });
                await sendToChat(
                    'systems/dark-heresy-1e/template/chat/initiative.hbs',
                    {
                        roll: roll,
                        rollHtml: await roll.render(),
                    }
                );
            } catch (err) {
                if (typeof err === 'string') {
                    alert(err);
                }
                throw err;
            }
        });

        html.find('[data-action="add-item"]').click((evt) => {
            const type = evt.currentTarget.getAttribute('data-item-type');
            let defaults = JSON.parse(evt.currentTarget.getAttribute('data-item-defaults') || '{}');

            let data = {
                name: `New ${type}`,
                type: type,
                system: defaults,
            };
            this.actor.createEmbeddedDocuments("Item", [data], { renderSheet: true });
        });

        html.find('[data-action="attack"]').click(async (evt) => {
            const weaponID = evt.currentTarget.getAttribute('data-weapon');
            const item = this.actor.items.get(weaponID);
            await showAttackDialog(this.actor, item);
        });

        // todo - allow for damage modifiers, show a dialog
        html.find('[data-action="roll-damage"]').click(async (evt) => {
            const weaponID = evt.currentTarget.getAttribute('data-weapon');
            const item = this.actor.items.get(weaponID);

            let location = new Roll(`1d100`);
            await location.evaluate({ async: true });

            let attack = new AttackData(this.actor, item);
            attack.hitCount = 1;
            attack.hitLocations = [
                locationByNumber(location.result),
            ];
            attack.handlers = [
                ...keywords(item)
            ];
            await attack.calculateDamageRoll();

            if (attack.damage.dieCount > 0) {
                await showDamageDialog(attack);
            }
        });

        // todo - allow for parry modifiers, show a dialog
        html.find('[data-action="parry"]').click(async (evt) => {
            const weaponID = evt.currentTarget.getAttribute('data-weapon');
            const item = this.actor.items.get(weaponID);

            let parry = new ParryData(this.actor, item);
            await parry.doParry();
        });
    }

    /** @override */
    async getData() {
        const data = super.getData();

        data.items = {};
        for (let type of Object.keys(this.actor.itemTypes)) {
            let items = this.actor.itemTypes[type];

            // todo - sort
            switch(type) {
                case 'skill':
                    items = items
                        .sort((a, b) => a.displayName.localeCompare(b.displayName))
                        .map((skill) => {
                            skill.totalRollValue = skill.model.totalRollValue;
                            return skill;
                        });
                    break;
                case 'characteristic':
                    items = items
                        .sort((a, b) => a.system.position - b.system.position)
                        .map((a) => {
                            if (a.system.has_bonus) {
                                a.bonus = Math.floor(a.model.total / 10);
                                a.remainder = (a.model.total % 10);
                            }
                            return a;
                        });
                    break;
                default:
                    items = items
                        .sort((a, b) => a.displayName.localeCompare(b.displayName));
                    break;
            }
            data.items[type] = items;
        }

        let columnLength = Math.floor(this.actor.itemTypes.talent.length / 3);
        data.talentColumns = [
            data.items.talent.slice(0, columnLength),
            data.items.talent.slice(columnLength, columnLength * 2),
            data.items.talent.slice(columnLength * 2),
        ];

        data.isGM = game.user.isGM;
        data.items.basicSkill = data.items.skill.filter((item) => item.system.is_basic);
        data.items.advancedSkill = data.items.skill.filter((item) => !item.system.is_basic);
        data.hitLocations = hitLocations.map((location) => {
            location.armour = this.actor.armourByLocation[location.key];
            return location;
        });

        for (let key of Object.keys(this.actor.modifiers)) {
            totalModifier(this.actor, key);
        }
        data.modifiers = this.actor.modifiers;

        data.allItems = [];
        for (let items of Object.values(this.actor.itemTypes)) {
            data.allItems.push(...items);
        }

        data.isPsyker = this.actor.totalPsyRating > 0;
        // console.log(data.actor.system);
        // data.system = data.data.system;
        // data.items = this.constructItemLists(data);
        // data.enrichment = await this._enrichment();
        return data;
    }

    /** @override */
    get template() {

        if (!game.user.isGM && this.actor.limited) {
            return "systems/dark-heresy-1e/template/sheet/actor/limited-sheet.hbs";
        } else {
            return "systems/dark-heresy-1e/template/sheet/actor/acolyte-sheet.hbs"
        }
    }

    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();
        if (this.actor.isOwner) {
            // buttons = [
            //     {
            //         label: 'Chat',
            //         class: 'send-to-chat',
            //         icon: 'fas fa-comment',
            //         onclick: async (evt) => this.item.model.sendToChat(),
            //     },
            //     ...buttons,
            // ];
        }
        return buttons;
    }

    _onItemCreate(event) {
        // event.preventDefault();
        // let header = event.currentTarget.dataset;
        //
        // let data = {
        //     name: `New ${game.i18n.localize(`TYPES.Item.${header.type.toLowerCase()}`)}`,
        //     type: header.type
        // };
        // this.actor.createEmbeddedDocuments("Item", [data], { renderSheet: true });
    }
}