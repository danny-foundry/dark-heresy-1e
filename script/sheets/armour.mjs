
export class DarkHeresyArmourSheet extends ItemSheet
{
    activateListeners(html) {
        super.activateListeners(html);
    }

    /** @override */
    async getData() {
        const data = super.getData();

        data.isEmbedded = data.item.isEmbedded;
        data.isGM = game.user.isGM;
        
        return data;
    }

    /** @override */
    get template() {
        if (!game.user.isGM && this.actor.limited) {
            return "systems/dark-heresy-1e/template/sheet/item/limited-sheet.hbs";
        } else {
            return "systems/dark-heresy-1e/template/sheet/item/armour-sheet.hbs";
        }
    }
}