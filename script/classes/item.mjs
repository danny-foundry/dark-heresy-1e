import {Skill} from "./items/skill.mjs";
import {BaseItem} from "./items/base.mjs";
import {Weapon} from "./items/weapon.mjs";
import {Characteristic} from "./items/characteristic.mjs";
import {Talent} from "./items/talent.mjs";

const models = {
    'skill': Skill,
    'melee_weapon': Weapon,
    'ranged_weapon': Weapon,
    'characteristic': Characteristic,
    'talent': Talent,
};
export class DarkHeresyItem extends Item
{
    #model = null;

    constructor(data, context) {
        super(data, context);
        const modelClass = models[this.type] || BaseItem;
        this.#model = new modelClass(this);
        this.#model.prepare();
    }

    get model() {
        return this.#model;
    }

    get availableSkillNames()
    {
        if (this.isEmbedded) {
            return this.parent.items
                .filter((item) => item.type === 'skill')
                .map((item) => item.name);
        }

        return Items.instance.filter((item) => item.type === 'skill')
            .map((item) => item.name);
    }

    get systemName() {
        return this.name
            .replace(')', '_')
            .replace('(', '_')
            .replace(/\s/g, '-')
            .toLowerCase();
    }

    get availableTalentNames()
    {
        if (this.isEmbedded) {
            return this.parent.items
                .filter((item) => item.type === 'talent')
                .map((item) => item.displayName);
        }

        return Items.instance.filter((item) => item.type === 'talent')
            .map((item) => item.displayName);
    }



    get displayName()
    {
        return this.#model.displayName;
    }


    async _preUpdate(changed, options, user) {
        await this.#model.preUpdateOrCreate(changed, options, user);
        await super._preUpdate(changed, options, user);
    }

    async _preCreate(data, options, user) {
        await this.#model.preUpdateOrCreate(data, options, user);
        await super._preCreate(data, options, user);
    }

    async sendToChat() {
        /*
        const item = new CONFIG.Item.documentClass(this.data._source);
        const html = await renderTemplate("systems/dark-heresy/template/chat/item.hbs", {item, data: item.system});
        const chatData = {
            user: game.user.id,
            rollMode: game.settings.get("core", "rollMode"),
            content: html
        };
        if (["gmroll", "blindroll"].includes(chatData.rollMode)) {
            chatData.whisper = ChatMessage.getWhisperRecipients("GM");
        } else if (chatData.rollMode === "selfroll") {
            chatData.whisper = [game.user];
        }
        ChatMessage.create(chatData);
         */
    }
}