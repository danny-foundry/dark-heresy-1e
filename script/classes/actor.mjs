import {updateDefaultEmbeds, getModifiers, modifiables, totalModifier} from "../helpers/actor.mjs";
import {deepGet} from "../helpers/arrays.mjs";

export class DarkHeresyActor extends Actor
{
    _modifiers = {};

    prepareDerivedData()
    {
        super.prepareDerivedData();
        this.system.health = {
            max: this.totalWounds,
            min: 0,
            value: this.totalWounds - this.system.damage
        };

        if (typeof(this.system.fatigue) !== 'object') {
            this.system.fatigue = {
                current: parseInt(this.system.fatigue),
                value: 0,
                max: 10,
                min: 0,
            };
        }

        this.system.fatigue.max = this.characteristics.T?.bonus || 0;
        this.system.fatigue.value = this.system.fatigue.max - this.system.fatigue.current;

        this._modifiers = {};
    }

    get availableXp()
    {
        return parseInt(this.system.xp?.earnt || 0) - parseInt(this.system.xp?.spent || 0);
    }

    get characteristicOptions() {
        let characteristics = {};

        for (let item of this.itemTypes.characteristic) {
            characteristics[item.system.abbreviation] = item.name;
        }

        return characteristics;
    }

    get characteristics() {
        let characteristics = {};

        for (let item of this.itemTypes.characteristic) {
            let mod = totalModifier(this, `characteristic.${item.system.abbreviation}`);
            let total = item.system.base + mod;
            let bonus = Math.floor(total / 10);

            characteristics[item.system.abbreviation] = {
                base: item.system.base,
                modifier: mod,
                bonus: bonus,
                total: total,
            };
        }

        return characteristics;
    }

    get woundsModifier() {
        return totalModifier(this, 'wounds');
    }

    get totalWounds() {
        return parseInt(this.system.wounds.base || 0) + this.woundsModifier;
    }

    get psyModifier() {
        return totalModifier(this, 'psy_modifier');
    }

    get psyRatingModifier() {
        return totalModifier(this, 'psy_rating');
    }

    get totalPsyRating() {
        return parseInt(this.system.psy_rating.base) + this.psyRatingModifier;
    }

    get totalInitiativeMod() {
        return this.characteristics.Ag.bonus + totalModifier(this, 'initiative');
    }

    get modifiers() {
        if (this._modifiers === undefined) {
            this._modifiers = {};
        }

        if (!Object.keys(this._modifiers).length) {
            this._modifiers = getModifiers(this);
            for (let key of Object.keys(this._modifiers)) {
                if (!key.startsWith('characteristic.')) {
                    continue;
                }
                totalModifier(this, key);
            }

            for (let key of Object.keys(this._modifiers)) {
                if (key.startsWith('characteristic.')) {
                    continue;
                }
                totalModifier(this, key);
            }
        }
        return this._modifiers;
    }

    get modifiables() {
        return modifiables(this);
    }

    get armourByLocation() {
        let locations = {
            head: 0,
            body: 0,
            left_arm: 0,
            right_arm: 0,
            left_leg: 0,
            right_leg: 0,
        }

        for (let item of this.items) {
            if (item.type !== 'armour') {
                continue;
            }
            if (!item.system.is_equipped) {
                continue;
            }

            for (let location of Object.keys(locations)) {
                locations[location] = Math.max(locations[location], item.system[location]);
            }
        }

        return locations;
    }

    get initiativeMod() {
        return Math.floor(this.characteristics.Ag.total / 10);
    }

    lookupValue(attribute) {
        if (attribute.startsWith('items.')) {
            return this.lookupItemValue(attribute.substring(6));
        }

        return deepGet(this.system, attribute);
    }

    lookupItemValue(item) {
        const pos = item.indexOf('.');
        const type = item.substring(0, pos);
        const key = item.substring(pos + 1);

        return this.itemTypes[type].find((obj) => {
            return obj.displayName === key;
        });
    }

    updateEmbeddedItems() {
        updateDefaultEmbeds(this)
    }


    getRollData() {
        let data = super.getRollData();

        data.initiative = this.totalInitiativeMod;

        return data;
    }

    async _preCreate(data, options, user) {
        // let basicSkills = [];
        // for (let item of Items.instance) {
        //     console.log(item);
        //     if (item.type === 'basic-skill') {
        //         basicSkills.push(item.clone().data);
        //     }
        // }

        // let initData = {
        //     "prototypeToken.bar1": { attribute: "wounds" },
        //     "prototypeToken.bar2": { attribute: "fate" },
        //     "prototypeToken.name": data.name,
        //     "prototypeToken.displayName": CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        //     "prototypeToken.displayBars": CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER
        //
        // };
        // if (data.type === "acolyte") {
        //     initData["prototypeToken.actorLink"] = true;
        //     initData["prototypeToken.disposition"] = CONST.TOKEN_DISPOSITIONS.FRIENDLY;
        // }
        // this.updateSource({ items: basicSkills });
    }
}