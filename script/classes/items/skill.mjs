import {BaseItem} from "./base.mjs";
import {slug} from "../../helpers/strings.mjs";
import {evaluateModifier, getModifiers} from "../../helpers/actor.mjs";

export class Skill extends BaseItem
{
    get levelName()
    {
        switch(parseInt(this._item.system.level)) {
            case 0:
                if (this._item.system.is_basic) {
                    return this._item.system.characteristic + ' / 2';
                }
                return '—';
            case 1:
                return this._item.system.characteristic;
            case 2:
                return this._item.system.characteristic + ' + 10'
            case 3:
                return this._item.system.characteristic + ' + 20';
            default:
                return '—';
        }
    }

    get chanceColour() {
        if (this.totalRollValue < 33) {
            return 'red';
        }
        if (this.totalRollValue < 66) {
            return 'yellow';
        }

        return 'green';
    }

    get totalRollValue()
    {
        return Math.floor(this.rollValue.reduce((carry, item) => carry + item.value, 0));
    }

    get rollValue()
    {
        let values = [];
        
        let characteristic = this._item.parent.lookupValue(`items.characteristic.${this._item.system.characteristic}`)?.model.rollValue || [];
        let characteristicTotal = (characteristic || []).reduce((carry, mod) => carry + mod.value, 0);

        if (parseInt(this._item.system.level) === 0) {
            if (this._item.system.is_basic) {
                return [
                    ...characteristic,
                    { label: `${this._item.displayName}, untrained`, value: -(characteristicTotal / 2) },
                ];
            } else {
                return [
                    { label: `${this._item.displayName}, untrained`, value: -1000 },
                ];
            }
        }

        values.push(...characteristic);
        values.push({ label: this._item.displayName, value: ((this._item.system.level - 1) * 10 )});

        let keys = [
            slug(this._item.system.name),
        ];
        if (this._item.system.subtype) {
            keys.push(slug(this._item.name));
        }

        for (let key of keys) {
            for (let modifier of evaluateModifier(this._item.parent, `skill.${key}`)) {
                values.push({ label: modifier.source_name, value: modifier.evaluated });
            }
        }

        return values;
    }


    async preUpdateOrCreate(data, options, user) {
        if (data.system?.name || data.system?.subtype) {
            let name = data.system?.name || this._item.system?.name;
            let subtype = data.system?.subtype;
            if (subtype === undefined) {
                subtype = this._item.system?.subtype;
            }

            if (subtype) {
                name = `${name} (${subtype})`;
            }

            if (name !== this._item.name) {
                data.name = name;
            }
        } else if (data.name) {
            let pattern = /(.*)\((.*)\)/;
            let matches = pattern.exec(data.name);

            data.system = data.system || {};
            if (!matches) {
                if (this._item.system.name !== data.name || this._item.system.subtype) {
                    data.system.name = data.name;
                    data.system.subtype = '';
                }

            } else {
                // this doesn't work on create :(
                if (this._item.system.name !== matches[1] || this._item.system.subtype !== matches[2]) {
                    data.system.name = matches[1];
                    data.system.subtype = matches[2];
                }
            }
        }
    }
}
