export class BaseItem
{
    _item = null;

    constructor(item) {
        this._item = item;
    }

    prepare() {

    }

    get displayName() {
        return this._item.name;
    }

    get lookupName()
    {
        return this._item.name;
    }

    get rollValue()
    {
        console.error(`${this._item.name}: Cannot calculate roll value for type ${this._item.type}`);
    }

    async sendToChat()
    {
        const html = await this._chatMessageContent();
        const chatData = {
            user: game.user.id,
            rollMode: game.settings.get("core", "rollMode"),
            content: html
        };
        if (["gmroll", "blindroll"].includes(chatData.rollMode)) {
            chatData.whisper = ChatMessage.getWhisperRecipients("GM");
        } else if (chatData.rollMode === "selfroll") {
            chatData.whisper = [game.user];
        }
        ChatMessage.create(chatData);
    }

    async _chatMessageContent() {
        return await renderTemplate("systems/dark-heresy-1e/template/chat/item.hbs", {item: this._item, data: this._item.system});
    }

    async preUpdateOrCreate(data, options, user) {

    }
}