import {BaseItem} from "./base.mjs";
import {evaluateModifier, getModifiers, totalModifier} from "../../helpers/actor.mjs";

export class Characteristic extends BaseItem
{
    get displayName()
    {
        return `${this._item.system.abbreviation}`;
    }

    get total()
    {
        if (!this._item.isEmbedded) {
            return 0;
        }

        let base = this._item.system.base;
        let mod = totalModifier(this._item.parent, `characteristic.${this._item.system.abbreviation}`);

        return base + mod;
    }

    get lookupName()
    {
        return `${this._item.system.abbreviation}`;
    }
    
    get rollValue()
    {
        let values = [];
        
        values.push({
            label: this._item.name,
            value: this._item.system.base,
        });

        for (let modifier of evaluateModifier(this._item.parent, `characteristic.${this._item.system.abbreviation}`)) {
            values.push({ label: modifier.source_name, value: modifier.evaluated });
        }

        // for (let modifier of this._item.parent.modifiers[`characteristic.${this._item.system.abbreviation}`] ?? []) {
        //     values.push({ label: modifier.source_name, value: modifier.value });
        // }

        return values;
    }
}
