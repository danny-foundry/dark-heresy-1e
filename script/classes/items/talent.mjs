import {BaseItem} from "./base.mjs";

export class Talent extends BaseItem
{
    prepare() {
        if (!this._item.system.name) {
            if (!this._item.id) {
                // just been created, has no id, can't update it
                return;
            }

            let pattern = /(.*)\((.*)\)/;
            let matches = pattern.exec(this._item.name);
            if (matches) {
                this._item.update({
                    system: {
                        _id: this._item._id,
                        name: matches[1],
                        subtype: matches[2],
                    }
                })
            } else {
                this._item.update({
                    system: {
                        _id: this._item._id,
                        name: this._item.name,
                        subtype: '',
                    }
                })
            }
        }
    }

    get hasRoll() {
        return Boolean(this._item.system.roll);
    }

    get expandedDescription()
    {
        return this._item.system.description.replace('{level}', this._item.system.level);
    }

    get displayName()
    {
        if (this._item.system.extra) {
            return `${this._item.name} (${this._item.system.extra})`;
        }
        return super.displayName;
    }

    get lookupName()
    {
        if (this._item.system.extra) {
            return `${this._item.name} (${this._item.system.extra})`;
        }

        return super.lookupName;
    }

    async preUpdateOrCreate(data, options, user) {
        if (data.system?.name || data.system?.subtype) {
            let name = data.system?.name || this._item.system?.name;
            let subtype = data.system?.subtype;
            if (subtype === undefined) {
                subtype = this._item.system?.subtype;
            }

            if (subtype) {
                name = `${name} (${subtype})`;
            }

            if (name !== this._item.name) {
                data.name = name;
            }
        } else if (data.name) {
            let pattern = /(.*)\((.*)\)/;
            let matches = pattern.exec(data.name);

            data.system = data.system || {};
            if (!matches) {
                if (this._item.system.name !== data.name || this._item.system.subtype) {
                    data.system.name = data.name;
                    data.system.subtype = '';
                }

            } else {
                // this doesn't work on create :(
                if (this._item.system.name !== matches[1] || this._item.system.subtype !== matches[2]) {
                    data.system.name = matches[1];
                    data.system.subtype = matches[2];
                }
            }
        }
    }
}
