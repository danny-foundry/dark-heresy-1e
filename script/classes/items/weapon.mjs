import {BaseItem} from "./base.mjs";
import {ucWords} from "../../helpers/strings.mjs";

export class Weapon extends BaseItem
{
    prepare() {
        super.prepare();

        this._item.system.ammo = this._item.system.ammo || {
            "standard": {
                "name": "Standard",
                "available": 0,
                "reusable": false,
                "empty": 0
            },
            "dumdum": {
                "usable": false,
                "available": 0,
                "reusable": false
            },
            "hotshot": {
                "usable": false,
                "available": 0,
                "reusable": true,
                "empty": 0
            },
            "inferno": {
                "usable": false,
                "available": 0,
                "reusable": false
            },
            "manstopper": {
                "usable": false,
                "available": 0,
                "reusable": false
            }
        };
    }

    get keywordsList()
    {
        return Object.keys(this._item.system.keywords)
            .filter((key) => this._item.system.keywords[key])
            .map((keyword) => ucWords(keyword));
    }

    get canAttack()
    {
        return !this.usesAmmo || this.currentAmmoLevel > 0;
    }

    get canParry()
    {
        return this._item.type === 'melee_weapon';
    }

    get damageTypeLabel()
    {
        switch (this._item.system.type) {
            case 'R':
                return 'Rending';
            case 'I':
                return 'Impact';
            case 'X':
                return 'Explosive';
            case 'E':
                return 'Energy';
            default:
                this._item.system.type;
        }
    }

    get ammoUsageByFireMode()
    {
        if (!this._item.system.rate_of_fire) {
            return {
                single: 1,
                semi: null,
                full: null,
            }
        }

        let usage = this._item.system.rate_of_fire.split('/');

        return {
            single: usage[0] === 'S' ? 1 : null,
            semi: parseInt(usage[1]) || null,
            full: parseInt(usage[2]) || null,
        };
    }

    get usesAmmo()
    {
        return this._item.type === 'ranged_weapon' && !!this._item.system.clip;
    }

    get reloadText()
    {
        let reload = this._item.system.reload.toLowerCase();
        if (reload === 'half') {
            return 'half action';
        }
        if (reload === 'full') {
            return '1 full action';
        }

        let actions = parseInt(reload.replace('full', ''));
        return `${actions} full actions`;
    }

    get parsedDamage()
    {
        const pattern = /(\d+)[dD](\d+)([+-]\d+)?/;
        const matches = pattern.exec(this._item.system.damage);

        return {
            dieCount: parseInt(matches[1] || 0),
            dieSize: parseInt(matches[2] || 10),
            modifier: parseInt(matches[3] || 0),
            dropLowest: 0,
            formula() {
                let damageFormula = `${this.dieCount}d${this.dieSize}`;
                if (this.dropLowest > 0) {
                    damageFormula += `dl${this.dropLowest}`;
                }

                if (this.modifier >= 0) {
                    damageFormula += '+' + this.modifier;
                } else {
                    damageFormula += this.modifier;
                }

                return damageFormula;
            }
        }
    }

    expendAmmo(amount)
    {
        if (!this.usesAmmo) {
            return;
        }

        this._item.update({
            system: {
                current_ammo: this.currentAmmoLevel - amount,
            }
        });
    }

    get currentAmmoLevel()
    {
        if (this._item.system.current_ammo === null) {
            return this._item.system.clip;
        }
        return this._item.system.current_ammo;
    }

    get hasSemiAuto()
    {
        const ammo = this.ammoUsageByFireMode.semi;
        if (ammo === null) {
            return false;
        }

        return this.currentAmmoLevel >= ammo;
    }

    get hasFullAuto()
    {
        const ammo = this.ammoUsageByFireMode.full;
        if (ammo == null) {
            return false;
        }

        return this.currentAmmoLevel >= ammo;
    }

    async _chatMessageContent() {
        return await renderTemplate("systems/dark-heresy-1e/template/chat/weapon.hbs", {item: this._item, data: this._item.system});
    }
}
