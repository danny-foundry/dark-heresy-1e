
export const hitLocations = [
    { name: 'Head', class: 'head', min: 1, max: 10, key: 'head' },
    { name: 'Body', class: 'body', min: 31, max: 70, key: 'body' },
    { name: 'Left Arm', class: 'left-arm', min: 21, max: 30, key: 'left_arm' },
    { name: 'Right Arm', class: 'right-arm', min: 11, max: 20, key: 'right_arm' },
    { name: 'Left Leg', class: 'left-leg', min: 86, max: 100, key: 'left_leg' },
    { name: 'Right Leg', class: 'right-leg', min: 71, max: 85, key: 'right_leg' },
];

export function locationFromAttackRoll(roll)
{
    return locationByNumber(parseInt([...("" + roll).padStart(2, '0')].reverse().join('')));
}

export function locationByNumber(number)
{
    return hitLocations.find((location) => location.min <= number && location.max >= number);
}