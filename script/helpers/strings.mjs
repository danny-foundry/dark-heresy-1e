export function slug(name)
{
    let pattern = /(.*)\((.*)\)/;
    let matches = pattern.exec(name);
    if (matches) {
        return matches[1].trim().toLowerCase().replace(' ', '-') + '_' + matches[2].trim().toLowerCase().replace(' ', '-');
    }
    return name.toLowerCase().replace(' ', '-');
}

export function ucFirst(first)
{
    if (!first) {
        return first;
    }

    return first[0].toUpperCase() + first.slice(1);
}

export function ucWords(keyword)
{
    return keyword
        .replace('_', ' ')
        .split(/\s/)
        .map((word) => ucFirst(word))
        .join(' ');
}