export async function sendToChat(template, data)
{
    const html = await renderTemplate(template, data);
    const chatData = {
        user: game.user.id,
        rollMode: game.settings.get("core", "rollMode"),
        content: html
    };
    if (["gmroll", "blindroll"].includes(chatData.rollMode)) {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM");
    } else if (chatData.rollMode === "selfroll") {
        chatData.whisper = [game.user];
    }
    ChatMessage.create(chatData);
}