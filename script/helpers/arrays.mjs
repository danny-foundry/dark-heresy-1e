export function keyBy(data, key) {
    let dictionary = {};

    for (let row of data) {
        const rowKey = deepGet(row, key.split("."));

        dictionary[rowKey] = row;
    }

    return dictionary;
}

export function deepGet(data, keys) {
    const key = keys.shift();
    if (!keys.length) {
        return data[key];
    }

    return deepGet(data[key] || {}, keys);
}