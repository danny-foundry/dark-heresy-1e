import {deepGet, keyBy} from "./arrays.mjs";
import {slug} from "./strings.mjs";

export function modifiables(actor)
{
    let modifiers = [];

    for (let item of actor.items) {
        if (item.type === 'skill') {
            let groupName = slug(item.system.name);
            let fullName = slug(item.name);
            if (!groupName) {
                let position = fullName.indexOf('_');
                if (position === -1) {
                    groupName = fullName;
                } else {
                    groupName = fullName.substring(0, position);
                }
            }

            modifiers.push(`skill.${fullName}`);
            if (fullName !== groupName && !modifiers.includes(`skill.${groupName}`)) {
                modifiers.push(`skill.${groupName}`);
            }
        } else if (item.type === 'characteristic') {
            modifiers.push(`characteristic.${item.system.abbreviation}`);
        }
    }

    modifiers.push('initiative');
    modifiers.push('wounds');
    modifiers.push('psy_rating');
    modifiers.push('psy_modifier');
    modifiers.push('ranged.damage');
    modifiers.push('ranged.critical');
    modifiers.push('melee.damage');
    modifiers.push('melee.critical');
    modifiers.push('crit.multiplier');

    return modifiers.sort();
}

function evaluateModifierValue(val)
{
    const pattern = /^[\d()-+\/\*\s\.]+$/;

    if (!pattern.test(val)) {
        return 0;
    }
    return eval(val);
}

export function evaluateModifier(actor, key)
{
    let modifiers = actor.modifiers;
    if (!modifiers) {
        return 0;
    }

    for (let modifier of modifiers[key] || []) {
        if (modifier.evaluated) {
            continue;
        }

        if (key.startsWith('characteristic.')) {
            modifier.evaluated = evaluateModifierValue(modifier.rawValue);
        } else {
            let val = modifier.rawValue;
            let characteristics = actor.characteristics;
            for (let characteristic of Object.keys(characteristics)) {
                let key = `characteristic.${characteristic}`;
                val = val.replace(key, characteristics[characteristic].bonus);
            }

            modifier.evaluated = evaluateModifierValue(val);
        }
    }

    return modifiers[key] || [];
}

export function totalModifier(actor, key)
{
    let evaluated = evaluateModifier(actor, key);
    return evaluated.reduce((carry, mod) => carry + mod.evaluated, 0);
}

export function getModifiers(actor)
{
    let modifiers = {};
    const pattern = /^([^=]+)=(.*)/;

    for (let item of actor.items) {
        if (!item.system.modifiers) {
            continue;
        }

        if (item.system.is_equipped === false) {
            continue;
        }

        let level = parseFloat(item.system.level || 0);
        try {
            const lines = item.system.modifiers.replace('{level}', level).split("\n");
            for (let line of lines) {
                if (!line.trim()) {
                    continue;
                }

                let matches = pattern.exec(line);
                if (matches && matches.length >= 3) {
                    modifiers[matches[1]] = modifiers[matches[1]] || [];
                    modifiers[matches[1]].push({
                        get value() {
                            console.warn('Modifier value access');
                            return evaluateModifierValue(matches[2]);
                        },
                        rawValue: matches[2],
                        source: item.id,
                        source_type: item.type,
                        source_name: item.name,
                    });
                }
            }

        } catch (err) {
            console.error(`Error parsing modifiers for ${actor.name} / ${item.type} / ${item.name}`);
            console.error(err);
        }
    }

    return modifiers;
}

function defaultEmbedsForAcolyte(actor)
{
    let currentSkills = keyBy(actor.itemTypes.skill, "name");
    let currentCharacteristics = keyBy(actor.itemTypes.characteristic, "system.abbreviation");
    let newItems = [];

    for (let item of Items.instance) {
        if (item.type === 'skill' && item.system.is_basic) {
            // check it exists
            if (currentSkills[item.name]) {
                continue;
            }

            newItems.push({
                name: item.name,
                type: item.type,
                system: item.system,
            });
        } else if (item.type === 'characteristic') {
            if (currentCharacteristics[item.system.abbreviation]) {
                continue;
            }

            newItems.push({
                name: item.name,
                type: item.type,
                system: item.system,
            });
        }
    }

    if (newItems.length) {
        actor.createEmbeddedDocuments("Item", newItems);
    }
}

export function updateDefaultEmbeds(actor)
{
    if (actor.type === 'acolyte') {
        defaultEmbedsForAcolyte(actor);
    }
}
