
import {actions} from "../keywords/actions.mjs";
import {modifiers} from "../keywords/modifiers.mjs";
import {keywords as special} from "../keywords/keywords.mjs";
import {locationFromAttackRoll, locationByNumber} from "../classes/ref/hit-locations.mjs";
import {sendToChat} from "./chat.mjs";
import {showDamageDialog} from "./dialogs.js";
import {rollDamage} from "./dice.mjs";
import {getAmmoById} from "../keywords/ammo.mjs";
import {totalModifier} from "./actor.mjs";

export function availableModifiers(actor, weapon)
{
    let available = {};

    for (let key of Object.keys(modifiers)) {
        let filteredModifiers = modifiers[key].filter((mod) => mod.callable.available(actor, weapon));
        if (filteredModifiers.length) {
            available[key] = modifiers[key].filter((mod) => mod.callable.available(actor, weapon));
        }
    }

    return available;
}

export function availableActions(actor, weapon)
{
    return actions
        .filter((action) => action.callable.available(actor, weapon))
        .map((action) => ({
            ...action,
            disabled: action.callable.disabled(actor, weapon),
        }));
}

export function keywords(weapon)
{
    let handlers = special.filter((keyword) => weapon.system.keywords[keyword.id]);
    let expected = Object.keys(weapon.system.keywords).filter((key) => weapon.system.keywords[key]);
    let actual = handlers.map((handler) => handler.id);

    expected = expected.filter((val) => val);
    actual = actual.filter((val) => val);

    if (expected.length !== actual.length) {
        console.warn(expected);
        console.warn(actual);
        console.error("failed to load weapon keywords");
        console.error("expected: " + expected.join(", "));
        console.error("actual: " + actual.join(", "));
        alert("Keyword load error");
    }

    return handlers;
}

export class ParryData
{
    actor = null;
    weapon = null;
    handlers = [];

    // roll configuration
    rollModifiers = [];
    target = 0;

    // roll data
    rollData = null;
    rollHtml = '';

    // results
    result = 0;
    margin = 0;
    degrees = 0;
    notices = [];
    success = false;

    constructor(actor, weapon) {
        this.actor = actor;
        this.weapon = weapon;
        this.handlers = [
            ...keywords(weapon)
        ];
    }

    async doParry() {
        this.rollModifiers.push({
            label: 'WS',
            value: this.actor.characteristics.WS.total,
        });

        if (this.actor.system.fatigue.current) {
            this.rollModifiers.push({
                label: 'Fatigue',
                value: -10,
            });
        }
        await this.#call(this.handlers, 'preParry', this);
        this.target = this.rollModifiers.reduce((carry, mod) => carry + mod.value, 0);

        this.rollData = new Roll(`1d100`);
        await this.rollData.evaluate({ async: true });
        this.result = this.rollData.total;
        this.rollHtml = await this.rollData.render();
        this.success = this.result <= this.target;

        if (this.success) {
            this.margin = this.target - this.result;
            this.degrees = Math.floor(this.margin / 10);
        }

        await this.#call(this.handlers, 'postParry', this);
        await sendToChat('systems/dark-heresy-1e/template/chat/parry.hbs', { attack: this });
    }

    async #call(handlers, callable, ...args) {
        for (let handler of handlers) {
            if (!handler.callable?.[callable]) {
                continue;
            }
            await handler.callable[callable](...args);
        }
    }
}

export class AttackData
{
    // Attack data
    actor = null;
    weapon = null;
    isMelee = false;
    isRanged = false;
    handlers = [];

    // Attack configuration
    selectedModifiers = [];
    selectedAction = null;
    ammo = null;
    ammoExpended = 1;
    targetModifiers = [];
    availableModifiers = {
    };
    availableActions = [];

    // Roll configuration
    target = 0;
    jamThreshold = 96;
    overheatThreshold = 101;
    rollData = null;
    rollDataHtml = '';

    // Results
    result = 0;
    margin = 0;
    degree = 0;
    isOverheat = false;
    isJam = false;
    isMiss = false;
    isHit = false;
    isTemplate = false;
    notices = [];
    damageNotices = [];

    hitCount = 0;
    hitLocations = [];

    // Damage configuration
    isSmoke = false;
    damage = {};
    damageFormula = "";

    // Damage results
    damageRollHtml = '';
    damageValues = [];
    penetrationModifier = 0;

    constructor(actor, weapon) {
        this.actor = actor;
        this.weapon = weapon;

        this.isMelee = weapon.type === 'melee_weapon';
        this.isRanged = !this.isMelee;
        this.availableModifiers = availableModifiers(this.actor, this.weapon);
        this.availableActions = availableActions(this.actor, this.weapon);
    }

    get penetration() {
        return parseInt(this.weapon.system.penetration) + parseInt(this.penetrationModifier);
    }

    async doAttackRoll(action, difficulty, selectedModifiers, ammo = 'standard') {
        this.ammo = getAmmoById(ammo);
        this.selectedAction = this.availableActions.find((actionDef) => actionDef.id === action);
        this.selectedModifiers = [];
        for (let key of Object.keys(this.availableModifiers)) {
            this.selectedModifiers.push(...this.availableModifiers[key].filter((modDef) => selectedModifiers.includes(modDef.id)));
        }

        if (!this.selectedAction) {
            console.error("Failed to load action " + action);
            return;
        }
        if (this.selectedModifiers.length !== selectedModifiers.length) {
            console.error("Failed to load all selected modifiers:");
            console.error("Loaded: " + this.selectedModifiers.map((mod) => mod.id).join(", "));
            console.error("Selected: " + selectedModifiers.join(", "));
            alert("Modifiers error");
            return;
        }

        this.handlers = [
            this.ammo,
            this.selectedAction,
            ...this.selectedModifiers,
            ...keywords(this.weapon),
        ]

        this.targetModifiers = [
            { label: this.weapon.system.characteristic, value: this.actor.characteristics[this.weapon.system.characteristic].total },
            { label: 'Difficulty', value: difficulty },
        ];

        if (this.actor.system.fatigue.current) {
            this.targetModifiers.push({
                label: 'Fatigue',
                value: -10,
            });
        }

        // actions to take before rolling - modifiers, expenditure etc
        await this.#call(this.handlers, 'preRoll', this);
        this.weapon.model.expendAmmo(this.ammoExpended);

        if (this.isTemplate) {
            await sendToChat('systems/dark-heresy-1e/template/chat/attack-template.hbs', { attack: this });
            this.hitCount = 1;
            this.hitLocations = [
                locationByNumber(50), // body
            ];

            if (this.isSmoke) {
                await sendToChat('systems/dark-heresy-1e/template/chat/damage-smoke.hbs', { attack: this, damage: this.damageFormula, hits: this.damageValues });
                return;
            }

            await this.#call(this.handlers, 'preDamage', this);
            await showDamageDialog(this);
            return;
        }

        await this.#rollAttack();
        await this.#call(this.handlers, 'postRoll', this);
        if (this.isOverheat) {
            // handle overheat
            await sendToChat("systems/dark-heresy-1e/template/chat/attack-overheated.hbs", { attack: this });
            return;
        }
        if (this.isJam) {
            // handle jam
            await sendToChat("systems/dark-heresy-1e/template/chat/attack-jammed.hbs", { attack: this });
            return;
        }
        if (this.isMiss) {
            // handle miss
            await sendToChat("systems/dark-heresy-1e/template/chat/attack-missed.hbs", { attack: this });
            return;
        }

        if (this.hitCount > 0) {
            this.hitLocations = [
                locationFromAttackRoll(this.result),
            ];
        }
        if (this.hitCount > 1) {
            let count = this.hitCount - this.hitLocations.length;

            this.hitLocations = [
                ...this.hitLocations,
                ...(await this.#rollHitLocations(count)).map((num) => locationByNumber(num)),
            ]
        }
        await sendToChat("systems/dark-heresy-1e/template/chat/attack-hit.hbs", { attack: this });

        if (this.isSmoke) {
            await sendToChat('systems/dark-heresy-1e/template/chat/damage-smoke.hbs', { attack: this, damage: this.damageFormula, hits: this.damageValues });
            return;
        }

        await this.calculateDamageRoll();
        if (this.damage.dieCount > 0) {
            await showDamageDialog(this);
        }
    }

    async calculateDamageRoll()
    {
        this.damage = this.weapon.model.parsedDamage;
        if (this.isMelee) {
            this.damage.modifier += parseInt(this.actor.characteristics.S.bonus);
        }

        let key = '';
        if (this.isMelee) {
            key = 'melee';
        }
        if (this.isRanged) {
            key = 'ranged';
        }

        // todo - support die expressions
        this.damage.modifier += totalModifier(this.actor, `${key}.damage`);
        this.damage.critModifier += totalModifier(this.actor, `${key}.critical`);
    }

    async doDamage()
    {
        await this.#call(this.handlers, 'preDamage', this);
        this.damageFormula = this.damage.formula();

        let damage = await rollDamage(this.damage, this.hitCount, this.target);

        this.damageValues = this.hitLocations.map((location, index) =>({
            location: location,
            damage: damage[index].damage || 0,
            righteousFuryChance: damage[index].righteousFuryChance || false,
            righteousDice: damage[index].righteousFuryDice || 0,
            righteousDamage: damage[index].righteousFuryDamage || 0,
            righteousSuccess: damage[index].righteousFurySuccess || false,
            damageHtml: damage[index].damageHtml || '',
            righteousFuryConfirmHtml: damage[index].righteousFuryConfirmHtml || '',
            righteousFuryDamageHtml: damage[index].righteousFuryDamageHtml || '',
        }));

        await this.#call(this.handlers, 'postDamage', this);

        await sendToChat('systems/dark-heresy-1e/template/chat/damage.hbs', { attack: this, penetration: this.penetration, damage: this.damageFormula, hits: this.damageValues });
    }

    async #rollHitLocations(count)
    {
        const rollData = new Roll(`${count}d100`);
        await rollData.evaluate({ async: true });
        return rollData.dice[0].results.map((result) => result.result);
    }

    async #rollAttack()
    {
        this.target = this.targetModifiers.reduce((carry, item) => carry + item.value, 0);
        this.rollData = new Roll("1d100");
        await this.rollData.evaluate({ async: true });

        this.result = this.rollData.total;
        this.margin = this.target - this.result;
        this.degree = Math.abs(Math.floor(this.margin / 10));
        this.rollDataHtml = await this.rollData.render();

        if (this.result >= this.overheatThreshold) {
            this.isOverheat = true;
        } else if (this.result >= this.jamThreshold) {
            this.isJam = true;
        } else if (this.margin < 0) {
            this.isMiss = true;
        } else {
            this.isHit = true;
            this.hitCount = 1;
        }
    }

    async #call(handlers, callable, ...args) {
        for (let handler of handlers) {
            if (!handler.callable?.[callable]) {
                continue;
            }
            await handler.callable[callable](...args);
        }
    }
}