import {hitLocations} from "../classes/ref/hit-locations.mjs";

function locationNumber(result)
{
    return parseInt([...("" + result).padStart(2, '0')].reverse().join(''))
}

function location(result)
{
    let hitbox = locationNumber(result);
    return hitLocations.find((location) => location.min <= hitbox && location.max >= hitbox)
}

async function rollD10()
{
    const rollData = new Roll("1d10", {});
    await rollData.evaluate({ async: true });

    return rollData.total;
}

async function rollD100()
{
    const rollData = new Roll("1d100", {});
    await rollData.evaluate({ async: true });

    return rollData.total;
}

export async function rollDamage(damage, hits, righteousFuryTarget)
{
    let results = [];
    let isD5 = false;
    if (damage.dieSize === 5) {
        damage.dieSize = 10;
        isD5 = true;
    }
    for (let i = 0; i < hits; i ++) {
        let righteousFuryDice = 0;
        let righteousFuryDamage = 0;
        let righteousFuryChance = false;

        let rollHtml = '';
        let righteousFuryConfirmHtml = '';
        let righteousFuryDamageHtml = '';

        const rollData = new Roll(damage.formula(), {});
        await rollData.evaluate({ async: true });
        rollHtml = await rollData.render();

        let total = 0;
        for (let die of rollData.dice[0].results) {
            if (!die.active) {
                continue;
            }

            if (die.result === 10) {
                righteousFuryChance = true;
            }

            if (isD5) {
                total += Math.ceil(die.result / 2);
            } else {
                total += die.result;
            }
        }

        if (righteousFuryChance) {
            let rollData = new Roll("1d100", {});
            await rollData.evaluate({ async: true });
            righteousFuryConfirmHtml = await rollData.render();
            let rfConfirm = rollData.total;

            if (rfConfirm <= righteousFuryTarget) {
                rollData = new Roll("1d10x", {});
                await rollData.evaluate({ async: true });
                righteousFuryDamage += rollData.total;
                righteousFuryDice = rollData.dice[0].length;
                righteousFuryDamageHtml = await rollData.render();
            }
        }

        total += damage.modifier + righteousFuryDamage;
        results.push({
            damage: total,
            righteousFurySuccess: righteousFuryDamage > 0,
            righteousFuryDice: righteousFuryDice,
            righteousFuryChance: righteousFuryChance,
            righteousFuryDamage: righteousFuryDamage,
            damageHtml: rollHtml,
            righteousFuryConfirmHtml,
            righteousFuryDamageHtml,
        });
    }

    if (isD5) {
        damage.dieSize = 5;
    }

    return results
}

export class AttackRoll
{
    #actor = null;
    #weapon = null;
    #modifiers = [];
    #action = '';

    constructor(actor, weapon, action, modifiers) {
        this.#actor = actor;
        this.#weapon = weapon;
        this.#modifiers = modifiers;
        this.#action = action;
    }

    async roll() {
        const base = this.#actor.characteristics[this.#weapon.system.characteristic]?.total || 0;
        const breakdown = [
            { label: this.#weapon.system.characteristic, value: base },
            ...this.#modifiers,
        ];
        let target = breakdown.reduce((carry, item) => carry + item.value, 0);
        let jamThreshold = 96;

        if (this.#action === 'full-auto' || this.#action === 'semi-auto' || this.#action === 'suppression') {
            jamThreshold = 94;
        }

        if (target >= jamThreshold) {
            target = jamThreshold - 1;
        }

        const rollData = new Roll("1d100", {});
        await rollData.evaluate({ async: true });
        const result = rollData.total;
        const degrees = Math.floor(Math.abs(target - result) / 10);
        let hits = 1;

        if (result <= target && this.#action === 'full-auto') {
            hits = degrees;
        }
        if (result <= target && this.#action === 'semi-auto') {
            hits = Math.floor(degrees / 2);
        }

        let hitLocations = [
            location(result).name,
        ];

        for (let i = 0; i < hits - 1; i ++) {
            const rollData = new Roll("1d100", {});
            await rollData.evaluate({ async: true });
            hitLocations.push(location(rollData.total).name);
        }
        // sample response
        return {
            result: result,
            target: target,
            success: result <= target,
            margin: degrees,
            jammed: result >= jamThreshold,
            modifiers: this.#modifiers,
            hits: hitLocations,
        };
    }
}

export class DiceRoll
{
    #pool = "";
    #actor = null;
    #components = [];

    #difficulty = 0;
    #rollData = null;
    #result = 0;
    #margin = 0;
    #degree = 0;
    #target = 0;
    #location = 0;
    #locationName = "";

    constructor(actor) {
        this.#actor = actor;
    }

    parse(pool) {
        this.#pool = pool;

        this.#components = this.parseRollText(this.#actor, this.#pool);
        this._logRollComponents();
        this.#target = this.#components.reduce((carry, component) => carry + component.value, 0);
    }

    async parseAndRoll(pool) {
        this.parse(pool);
        await this.rollDice();
        await this.toChatMessage();
    }

    parseRollText(actor, roll)
    {
        const components = [];
        const parts = roll.split("+");

        for (let part of parts) {
            let value = 0;
            part = part.trim();

            if (!isNaN(part)) {
                value = parseInt(part);
                components.push({
                    attribute: part,
                    label: part,
                    value: value,
                });
            } else {
                // lookup on character object
                components.push(...actor.lookupValue(part)?.model.rollValue);
            }
        }

        console.log(actor);
        console.log(actor.system.fatigue);
        if (actor.system.fatigue.current) {
            components.push({
                attribute: 'fatigue',
                label: 'Fatigued',
                value: -10,
            });
        }

        return components;
    }

    async rollDice() {
        const hitLocations = [
            { max: 10, location: 'Head' },
            { max: 20, location: 'Right Arm' },
            { max: 30, location: 'Left Arm' },
            { max: 70, location: 'Body' },
            { max: 85, location: 'Right Leg' },
            { max: 100, location: 'Left Leg' },
        ];

        await this.showRollOptionsDialog();

        this.#rollData = new Roll("1d100", {});
        await this.#rollData.evaluate({ async: true });

        this.#result = this.#rollData.total;
        this.#margin = this.#target - this.#result;

        if (this.#margin < 0) {
            this.#degree = Math.floor(-this.#margin / 10);
        } else {
            this.#degree = Math.floor(this.#margin / 10);
        }
        this.#location = locationNumber(this.#result);
        this.#locationName = hitLocations.find((item) => this.#location <= item.max).location;
    }

    async showRollOptionsDialog() {
        return new Promise(async (resolve, reject) => {
            let dialog = new Dialog({
                title: "Rolling " + this.#pool,
                content: await renderTemplate(
                    "systems/dark-heresy-1e/template/dialog/roll-options.hbs",
                    {
                        difficulty: this.#difficulty,
                    }
                ),
                buttons: {
                    roll: {
                        icon: '<i class="fas fa-check"></i>',
                        label: 'Roll',
                        callback: async (html) => {
                            this.#difficulty = parseInt(html.find('[data-field="difficulty"]')[0].value, 10);
                            this.#target += this.#difficulty;

                            // todo - update settings from HTML
                            resolve();
                        }
                    },
                    cancel: {
                        icon: '<i class="fas fa-times"></i>',
                        label: 'Cancel',
                        callback: () => reject(),
                    }
                },
                default: 'roll',
                close: () => reject(),
                render: html => {
                    // todo - bindings
                }
            }, {
                width: 200,
            });

            dialog.render(true);
        });
    }

    async toChatMessage() {
        let speaker = ChatMessage.getSpeaker();
        let chatData = {
            user: game.user.id,
            type: CONST.CHAT_MESSAGE_TYPES.ROLL,
            rollMode: game.settings.get("core", "rollMode"),
            speaker: speaker,
            flags: {
                "dark-heresy-1e.rollData": this.#rollData
            }
        };

        chatData.content = await renderTemplate("systems/dark-heresy-1e/template/chat/roll.hbs", {
            tokenID: speaker?.token,
            rollHtml: await this.#rollData.render(),
            config: {
                pool: this.#pool,
                components: [...this.#components, { label: 'Difficulty', value: this.#difficulty }],
            },
            result: {
                result: this.#result,
                margin: this.#margin,
                degree: this.#degree,
                target: this.#target,
                location: this.#location,
                locationName: this.#locationName,
                isSuccess: this.#result <= this.#target,
                isFailure: this.#result > this.#target,
            },
        });

        if (["gmroll", "blindroll"].includes(chatData.rollMode)) {
            chatData.whisper = ChatMessage.getWhisperRecipients("GM");
        } else if (chatData.rollMode === "selfroll") {
            chatData.whisper = [game.user];
        }

        ChatMessage.create(chatData);
    }

    _logRollComponents() {
        console.group('Rolling ' + this.#pool);
        for (let component of this.#components) {
            let val = component.value;
            if (val >= 0) {
                val = `+${val}`;
            }
            console.log(`${component.label}: ${val}`);
        }
        console.log(this.#target);
        console.groupEnd();
    }
}
