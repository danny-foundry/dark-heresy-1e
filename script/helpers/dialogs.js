import {AttackRoll, rollDamage} from "./dice.mjs";
import {sendToChat} from "./chat.mjs";
import {AttackData} from "./combat.mjs";
import {ammo} from "../keywords/ammo.mjs";
import {rollPhenomena} from "../keywords/psychic-phenomena.mjs";
import {totalModifier} from "./actor.mjs";

export async function showPsychicPowerDialog(actor, power)
{
    return new Promise(async (resolve, reject) => {
        let dialog = new Dialog({
            title: "Invoke " + power.name,
            content: await renderTemplate(
                "systems/dark-heresy-1e/template/dialog/psychic-power.hbs",
                {
                    actor: actor,
                    power: power,
                    psy_modifier: actor.psyModifier,
                    psy_rating: actor.totalPsyRating,
                }
            ),
            buttons: {
                apply: {
                    icon: '<i class="fas fa-check"></i>',
                    label: 'Invoke',
                    callback: async (html) => {
                        let dice = parseInt(html.find('[name="power"]').val());
                        if (dice > actor.totalPsyRating) {
                            dice = actor.totalPsyRating;
                        }

                        let modifier = parseInt(html.find('[name="modifier"]').val());
                        modifier += actor.psyModifier;

                        let will = actor.characteristics.Will.bonus;
                        let rollData = new Roll(`${dice}d10 + ${will} + ${modifier}`);
                        await rollData.evaluate({ async: true });
                        let phenomena = [];
                        let success = rollData.total >= power.system.threshold;
                        let margin = rollData.total - power.system.threshold;

                        for (let die of rollData.dice[0].results) {
                            if (die.result !== 9) {
                                continue;
                            }

                            phenomena.push(await rollPhenomena());
                        }

                        let hasOverbleed = Boolean(power.system.overbleed);
                        let overbleed = 0;
                        if (hasOverbleed && success) {
                            overbleed = Math.floor(margin / power.system.overbleed);
                        }
                        hasOverbleed = overbleed > 0;

                        // todo - need to be able to re-roll phenomena
                        // todo - perils causes gain of 1 corruption point
                        await sendToChat(
                            'systems/dark-heresy-1e/template/chat/psychic-power.hbs',
                            {
                                actor,
                                power,
                                rollData,
                                phenomena,
                                success,
                                margin: margin,
                                rollHtml: await rollData.render(),
                                hasOverbleed,
                                overbleed,
                            }
                        );
                        resolve();
                    }
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: 'Cancel',
                    callback: () => resolve(),
                }
            },
            default: 'apply',
            close: () => resolve(),
            render: html => {
                // todo - bindings
            }
        }, {
            width: 900,
        });

        dialog.render(true);
    });
}

export async function showTakeDamageDialog(actor, location)
{
    const armour = actor.armourByLocation[location.key];

    return new Promise(async (resolve, reject) => {
        let dialog = new Dialog({
            title: "Take Damage",
            content: await renderTemplate(
                "systems/dark-heresy-1e/template/dialog/take-damage.hbs",
                {
                    actor: actor,
                    location: location,
                    armour: armour,
                }
            ),
            buttons: {
                apply: {
                    icon: '<i class="fas fa-check"></i>',
                    label: 'Apply',
                    callback: async (html) => {
                        let amount = parseInt(html.find('[name="amount"]').val());
                        let extraCrit = parseInt(html.find('[name="extra_crit"]').val());
                        let ignoreToughness = html.find('[name="ignore_toughness"]').is(':checked');
                        let ignoreArmour = html.find('[name="ignore_armour"]').is(':checked');
                        let ap = parseInt(html.find('[name="ap"]').val());
                        let type = html.find('[name="type"]').val();

                        let damageTaken = amount;
                        let unpenetratedArmour = Math.max(0, armour - ap);

                        if (!ignoreArmour) {
                            damageTaken = damageTaken - unpenetratedArmour;
                        }

                        if (!ignoreToughness) {
                            damageTaken = damageTaken - actor.characteristics.T.bonus;
                        }

                        if (damageTaken < 1) {
                            await sendToChat('systems/dark-heresy-1e/template/chat/take-no-damage.hbs', { actor, location, amount, ignoreToughness, ignoreArmour, ap, type, damageTaken, unpenetratedArmour });
                            resolve();
                            return;
                        }

                        let totalDamage = parseInt(actor.system.damage || 0) + damageTaken;
                        let critical = totalDamage - actor.totalWounds;
                        let multiplier = totalModifier(actor, 'critical.multiplier');
                        let actualCrit = critical;

                        if (critical > 0) {
                            critical += extraCrit;
                            damageTaken += extraCrit;
                            actualCrit = critical * multiplier;
                        }

                        actor.update({
                            system: {
                                damage: parseInt((actor.system.damage || 0)) + damageTaken,
                            }
                        });


                        if (critical > 0) {
                            // todo lookup critical effect and send to chat
                            await sendToChat('systems/dark-heresy-1e/template/chat/take-critical-damage.hbs', { crit: '', criticalDamage: critical, actor, location, amount, ignoreToughness, ignoreArmour, ap, type, damageTaken, unpenetratedArmour, extraCrit, actualCrit });
                            resolve();
                            return;
                        }

                        await sendToChat('systems/dark-heresy-1e/template/chat/take-damage.hbs', { actor, location, amount, ignoreToughness, ignoreArmour, ap, type, damageTaken, unpenetratedArmour });
                        resolve();
                    }
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: 'Cancel',
                    callback: () => resolve(),
                }
            },
            default: 'apply',
            close: () => resolve(),
            render: html => {
                // todo - bindings
            }
        }, {
            width: 900,
        });

        dialog.render(true);
    });
}

export async function showAttackDialog(actor, weapon)
{
    const attack = new AttackData(actor, weapon);

    return new Promise(async (resolve, reject) => {
        let dialog = new Dialog({
            title: "Attack",
            content: await renderTemplate(
                "systems/dark-heresy-1e/template/dialog/attack.hbs",
                {
                    actor: actor,
                    weapon: weapon,
                    isMelee: attack.isMelee,
                    isRanged: attack.isRanged,
                    attack: attack,
                    hasSemiAuto: weapon.model.hasSemiAuto,
                    hasFullAuto: weapon.model.hasFullAuto,

                    usesAmmo: weapon.model.usesAmmo,
                    ammoTypes: ammo,
                    modifiers: attack.availableModifiers,
                    actions: attack.availableActions,
                }
            ),
            buttons: {
                roll: {
                    icon: '<i class="fas fa-check"></i>',
                    label: 'Roll',
                    callback: async (html) => {
                        const action = html.find('[name="action"]').val();
                        const difficulty = html.find('[name="difficulty"]').val();
                        const ammo = html.find('[name="ammo"]:checked').val();

                        const selectedModifiers = [...html.find('[data-field="modifiers"]:checked')]
                            .map(
                                (mod) => mod.getAttribute('data-modifier')
                            )
                            .filter((mod) => mod);

                        await attack.doAttackRoll(action, parseInt(difficulty), selectedModifiers, ammo);
                        resolve();
                    }
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: 'Cancel',
                    callback: () => resolve(),
                }
            },
            default: 'roll',
            close: () => resolve(),
            render: html => {
                // todo - bindings
            }
        }, {
            width: 900,
        });

        dialog.render(true);
    });
}

export async function showDamageDialog(attack)
{
    return new Promise(async (resolve, reject) => {
        let dialog = new Dialog({
            title: "Attack",
            content: await renderTemplate(
                "systems/dark-heresy-1e/template/dialog/damage.hbs",
                {
                    attack: attack,
                }
            ),
            buttons: {
                roll: {
                    icon: '<i class="fas fa-check"></i>',
                    label: 'Roll',
                    callback: async (html) => {
                        await attack.doDamage();
                        resolve();
                    }
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: 'Cancel',
                    callback: () => resolve(),
                }
            },
            default: 'roll',
            close: () => resolve(),
            render: html => {
                // todo - bindings
            }
        }, {
            width: 200,
        });

        dialog.render(true);
    });
}