export const modifiers = {
    attacker: [
        {
            // automatic?
            id: 'untrained',
            name: 'Untrained (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Untrained', value: -20 }),
            }
        },
        {
            id: 'called',
            name: 'Called shot (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Called shot', value: -20 }),
            }
        },
        {
            id: 'harsh-weather',
            name: 'Harsh weather (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Harsh weather', value: -20 }),
            }
        },
        {
            id: 'attacker-prone',
            name: 'Attacker prone (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Firing from prone', value: -20 }),
            }
        },

        // Melee
        {
            id: 'darkness-melee',
            name: 'Darkness (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => weapon.type === 'melee_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Darkness', value: -20 }),
            }
        },
        {
            id: 'high-ground',
            name: 'High ground (+10)',
            modifier: +10,
            callable: {
                available: (actor, weapon) => weapon.type === 'melee_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'High ground', value: +10 }),
            }
        },

        // Ranged
        {
            id: 'pinned',
            name: 'Pinned (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Pinned', value: -20 }),
            }
        },
        {
            id: 'darkness-ranged',
            name: 'Darkness (-30)',
            modifier: -30,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Darkness', value: -30 }),
            }
        },
        {
            id: 'fog',
            name: 'Fog / Mist/ Shadows (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Fog', value: -20 }),
            }
        },
    ],
    defender: [
        {
            id: 'defending',
            name: 'Opponent defending (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target defends', value: -20 }),
            }
        },
        {
            id: 'full-cover',
            name: 'Full cover (-30)',
            modifier: -30,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target in full cover', value: -30 }),
            }
        },
        {
            id: 'defender-prone',
            name: 'Target prone (+10)',
            modifier: +10,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target is prone', value: +10 }),
            }
        },
        {
            id: 'stunned',
            name: 'Target stunned (+20)',
            modifier: +20,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target is stunned', value: +20 }),
            }
        },
        {
            id: 'unaware',
            name: 'Target unaware (+30)',
            modifier: +30,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target is unaware', value: +30 }),
            }
        },

        // Ranged
        {
            id: 'in-melee',
            name: 'Opponent in melee (-20)',
            modifier: -20,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Target is in melee', value: -20 }),
            }
        },
    ],
    aiming: [
        {
            id: 'half-aim',
            name: 'Half round aim (+10)',
            modifier: +10,
            group: 'aim',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => !weapon.system.keywords.inaccurate,
                preRoll: (attack) => {
                    attack.targetModifiers.push({ label: 'Half round aim', value: 10 });
                    if (attack.weapon.system.keywords.accurate) {
                        attack.targetModifiers.push({ label: 'Accurate', value: 10 });
                    }
                },
            }
        },
        {
            id: 'full-aim',
            name: 'Full round aim (+20)',
            modifier: +20,
            group: 'aim',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => !weapon.system.keywords.inaccurate,
                preRoll: (attack) => {
                    attack.targetModifiers.push({ label: 'Full round aim', value: 20 });
                    if (attack.weapon.system.keywords.accurate) {
                        attack.targetModifiers.push({ label: 'Accurate', value: 10 });
                    }
                },
            }
        },
    ],
    offhand: [
        {
            id: 'offhand',
            name: 'Offhand (-20)',
            modifier: -20,
            group: 'offhand',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Offhand', value: -20 }),
            }
        },
        {
            id: 'offhand-ambidextrous',
            name: 'Offhand, ambidextrous (-10)',
            modifier: -10,
            group: 'offhand',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Offhand (ambidextrous)', value: -10 }),
            }
        },
    ],
    terrain: [
        {
            id: 'difficult-terrain',
            name: 'Difficult terrain (-10)',
            modifier: -10,
            group: 'terrain',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Difficult terrain', value: -10 }),
            }
        },
        {
            id: 'arduous-terrain',
            name: 'Arduous terrain (-30)',
            modifier: -30,
            group: 'terrain',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => true,
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Arduous terrain', value: -30 }),
            }
        },
    ],
    range: [
        {
            id: 'short-range',
            name: '< 1/2 range (+10)',
            modifier: +10,
            group: 'range',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Short range', value: 10 }),
            }
        },
        {
            id: 'long-range',
            name: 'Long range (over 2x) (-10)',
            modifier: -10,
            group: 'range',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => {
                    attack.targetModifiers.push({ label: 'Long range', value: -10 });

                },
                preDamage: (attack) => {
                    if (attack.weapon.system.keywords.scatter) {
                        attack.damageNotices.push({ text: 'Armour doubled due to spread.' });
                    }
                }
            }
        },
        {
            id: 'extreme-range',
            name: 'Extreme range (over 3x) (-30)',
            modifier: -30,
            group: 'range',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                preRoll: (attack) => {
                    attack.targetModifiers.push({ label: 'Extreme range', value: -30 });
                },
                preDamage: (attack) => {
                    if (attack.weapon.system.keywords.scatter) {
                        attack.damageNotices.push({ text: 'Armour doubled due to spread.' });
                    }
                }
            }
        },
        {
            id: 'point-blank',
            name: 'Point blank (+30)',
            modifier: +30,
            group: 'range',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'ranged_weapon',
                postRoll: (attack) => {
                    attack.targetModifiers.push({ label: 'Point blank', value: +30 });
                    if (attack.weapon.system.keywords.scatter && attack.margin >= 0) {
                        let extraHits = Math.floor(attack.degree / 2);
                        attack.hitCount += extraHits;
                        attack.notices.push({ text: `Spread results in ${extraHits} extra hits.` });
                    }
                },
            }
        },
    ],
    outnumber: [
        {
            id: 'outnumbered-2-1',
            name: 'Outnumber [2-to-1] (+10)',
            modifier: +10,
            group: 'outnumber',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'melee_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Two on one', value: +10 }),
            }
        },
        {
            id: 'outnumbered-3-1',
            name: 'Outnumber [3-to-1] (+20)',
            modifier: +20,
            group: 'outnumber',
            isExclusive: true,
            callable: {
                available: (actor, weapon) => weapon.type === 'melee_weapon',
                preRoll: (attack) => attack.targetModifiers.push({ label: 'Three on one', value: +20 }),
            }
        },
    ],
};