function lookup(table, number) {
    return table.find((row) => row.min <= number && row.max >= number);
}

export async function rollPhenomena()
{
    let phenomenaRoll = new Roll(`1d100`);
    await phenomenaRoll.evaluate({ async: true });
    let phenomenon = lookup(phenomena, phenomenaRoll.total);

    let perilRoll = false;
    let peril = null;

    if (phenomenon.perils) {
        perilRoll = new Roll('1d100');
        await perilRoll.evaluate({ async: true });
        peril = lookup(perils, perilRoll.total);
    }

    return {
        roll: phenomenaRoll,
        rollHtml: await phenomenaRoll.render(),
        result: phenomenaRoll.total,
        title: phenomenon.title,
        text: phenomenon.text,
        isPerils: phenomenon.perils,

        perilRoll: perilRoll,
        perilRollHtml: perilRoll ? (await perilRoll.render()) : null,
        perilResult: perilRoll?.total,
        perilTitle: peril?.title,
        perilText: peril?.text,
    };
}

export const phenomena = [
    { min: 1, max: 3, title: "Dark Forebodings", text: "A very faint breeze blows past the Psyker and those near him, and everyone gets the eerie feeling that something unfortunate has just happened somewhere in the galaxy." },
    { min: 4, max: 5, title: "Warp Echo", text: "For a few seconds, voices and other noises cause echoes regardless of surroundings." },
    { min: 6, max: 8, title: "Ethereal Stench", text: "The air around the Psyker fills with a faint smell, which can either be pleasant or noxious." },
    { min: 9, max: 11, title: "Mounting Paranoia", text: "The Psyker gets an itch between his shoulder blades for a few moments." },
    { min: 12, max: 14, title: "Grave Chill", text: "The temperature drops sharply for a few seconds and a fine coating of frost covers everything within 3d10 metres of the Psyker." },
    { min: 15, max: 17, title: "Unnatural Aura", text: "All animals within 1d100 metres become spooked and restless." },
    { min: 18, max: 20, title: "Memory Worm", text: "All people within line of sight of the Psyker forget something trivial" },
    { min: 21, max: 23, title: "Spoilage", text: "Food goes off and drink goes stale in a radius of 5d10 metres." },
    { min: 24, max: 26, title: "Haunting Breeze", text: "Moderate winds whip up around the Psyker for a few seconds, blowing very light objects about within 3d10 metres." },
    { min: 27, max: 29, title: "Veil of Darkness", text: "For a brief moment (effectively the remainder of the round) it seems to everyone within 3d10 metres of the Psyker that night has fallen, plunging the area into darkness." },
    { min: 30, max: 32, title: "Distorted Reflection", text: "Mirrors break and other reflective surfaces distort or ripple within 5d10 metres of the Psyker." },
    { min: 33, max: 35, title: "Breath Leech", text: "Everyone (including the Psyker) become short of breath for a round and cannot make any Run or Charge Actions." },
    { min: 36, max: 38, title: "Daemonic Mask", text: "For a fleeting moment the Psyker takes on a Daemonic appearance and gains a Fear Rating of 1 for the remainder of the Round, but also gains 1 Corruption Point." },
    { min: 39, max: 41, title: "Unnatural Decay", text: "All plants within 3d10 metres of the Psyker wither and die." },
    { min: 42, max: 44, title: "Spectral Gale", text: "Howling winds erupt around the Psyker, lifting him slightly into the air and forcing both him and anyone within 4d10 metres to make an Easy (+30) Agility Test or be knocked to the ground." },
    { min: 45, max: 47, title: "Bloody Tears", text: "Blood weeps from stone and wood within 3d10 metres of the Psyker. If there are any pictures of people or statues in this radius, they appear to be crying blood." },
    { min: 48, max: 50, title: "The Earth Protests", text: "The ground suddenly shakes and everyone (including the Psyker) within a 5d10 metre radius must make a Routine (+10) Agility Test or be knocked down." },
    { min: 51, max: 53, title: "Psy Discharge", text: "Static electricity fills the air for 6d10 metres causing hair to stand on end, while the Psyker rises 1d5 metres into the air, falling back to earth after a second or two." },
    { min: 54, max: 56, title: "Warp Ghosts", text: "Ghostly apparitions fill the air for 3d10 metres around the Psyker, flying around and howling in pain for a few brief moments. Everyone in the radius must make a WP Test or gain 1 Insanity Point." },
    { min: 57, max: 59, title: "Falling Upwards", text: "Everything within 2d10 metres of the Psyker (including him) rises 1d10 metres into the air as gravity briefly disappears before falling to the ground after a second or two." },
   { min: 60, max: 62, title: "Banshee Howl", text: "A deafening keening sounds out for a kilometre, shattering glass and forcing everyone in the area (including the Psyker) to make a Toughness Test or be deafened for 1d10 Rounds." },
    { min: 63, max: 65, title: "The Furies", text: "The Psyker is thrown to the ground by unseen hands and thrashes about for a few moments as winds howl about within 6d10 metres of him, lifting up light objects and forcing those in the area to make Agility Tests or be blown down." },
    { min: 66, max: 68, title: "Shadow of the Warp", text: "For a split second the world changes in appearance and everyone within 1d100 metres has a glimpse at the heart of the warp. Everyone in the area (including the Psyker) must make a WP Test or gain 1d5 Insanity Points." },
    { min: 69, max: 71, title: "Tech Scorn", text: "The machine spirits reject your unnatural ways. All tech devices within 5d10 metres malfunction momentarily and all ranged weapons Jam." },
    { min: 72, max: 74, title: "Warp Madness", text: "A violent ripple of discord causes all creatures within 2d10 metres (with the exception of the Psyker) to become Frenzied for a Round and gain a Corruption Point." },
    { min: 75, max: 100, title: "Perils of the Warp", text: "Invoking the Psychic Power calls down a maelstrom of warp energy power.", perils: true },
];

export const perils = [
    { min: 1, max: 5, title: 'The Gibbering', text: 'The Psyker screams in pain as uncontrolled warp energies surge through his unprotected mind. He must make a Willpower Test or gain 1d5 Insanity Points.' },
    { min: 6, max: 9, title: 'Warp Burn', text: 'A violent burst of energy from the warp smashes into the Psyker’s mind, sending him reeling. He is Stunned for 1d5 Rounds.' },
    { min: 10, max: 13, title: 'Psychic Concussion', text: 'With a crack of energy the Psyker is knocked unconscious for 1d5 Rounds and everyone within 3d10 metres must make a Willpower Test or be Stunned for a Round.' },
    { min: 14, max: 18, title: 'Psy-Blast', text: 'There is an explosion of power and the Psyker is thrown 1d10 metres into the air, falling to the ground (see page 210 for Falling Damage).' },
    { min: 19, max: 24, title: 'Soul Sear', text: 'Warp power courses through the Psyker’s body, scorching his very soul. The Psyker cannot use any powers for one hour and gains 5 Corruption Points.' },
    { min: 25, max: 30, title: 'Locked In', text: 'The power cages the Psyker’s mind in an ethereal prison. The Psyker falls to the ground in a catatonic state. Each Round thereafter, he must spend a Full Action to Test Willpower. On a success, his mind is freed and restored to his body.' },
    { min: 31, max: 38, title: 'Chronological Incontinence', text: 'Time warps around the Psyker. The character winks out of existence and reappears in 1d10 Rounds (or one minute if you’re using Narrative Time).' },
    { min: 39, max: 46, title: 'Psychic Mirror', text: 'The Psyker’s power is turned upon him. Resolve the power’s effects as normal but the power targets the Psyker instead. Should the power be of benefit, it instead deals 1d10+5 Energy Damage to the Psyker and the beneficial effect is cancelled. Armour offers the Psyker no protection against this Damage.' },
    { min: 47, max: 55, title: 'Warp Whispers', text: 'The ghostly voices of Daemons fill the air within 4d10 metres of the Psyker. Everyone in the area (including the Psyker) must make a Hard (–20) Willpower Test or gain 1d10 Corruption Points.' },
    { min: 56, max: 61, title: 'Vice Versa', text: 'The Psyker’s mind is thrown out of his body and into another nearby creature or person. The Psyker and a random being within 50 metres swap minds for 1d10 Rounds. This may include fellow Acolytes, or even enemy combatants. Each creature retains its Weapon Skill, Ballistic Skill, Intelligence, Perception, Willpower, and Fellowship during the swap, but gain the other Characteristics of the new body. Should either body be slain, the effect immediately ends. Both beings are strangely revolted by the process and are unable to meet each other’s gaze for some time afterwards. Each gains 1d5 Insanity Points for the experience. If there are no creatures within this range, the Psyker must make a Willpower Test or become catatonic for 1d5 Rounds whilst his mind wanders the warp and gains 1d5 Insanity Points.' },
    { min: 62, max: 67, title: 'Dark Summoning', text: 'A Lesser Daemon (see Chapter XII: Aliens, Heretics & Antagonists) pops into existence within 3d10 metres of the Psyker for 1d10 Rounds or until it is slain. It detests the Psyker and trains its attacks on the fool that summoned it.' },
    { min: 68, max: 72, title: 'Ethereal Storm', text: 'All sentient creatures (including the Psyker) within 1d100 metres take 1d10 Energy Damage that ignores Armour.' },
    { min: 73, max: 78, title: 'Blood Rain', text: 'A psychic storm erupts, covering an area of 5d10 metres. In addition to whipping winds and raining blood, any Psychic Powers used in the area automatically invoke the Perils of the Warp for 1d5 Rounds.' },
    { min: 79, max: 82, title: 'Cataclysmic Blast', text: 'The Psyker’s power overloads, arcing out in great bolts of warp energy. Anyone within 2d10 metres of him (including the Psyker) takes 1d10+5 Energy Damage and all of the Psyker’s clothing and gear are destroyed, leaving him naked and smoking on the ground.' },
    { min: 83, max: 86, title: 'Mass Possession', text: 'Daemons ravage the minds of every living thing within 1d100 metres for 2d10 Rounds. Every character in the area must Test Willpower at the start of their Turn. A failed Test indicates that the character must spend his entire Turn fighting off the attack and counts as helpless. Characters that also fail the Test gain 1d5 Corruption Points.' },
    { min: 87, max: 90, title: 'The Surly Bonds of Earth', text: 'Reality buckles and all gravity within 1d100 metres reverses for 1d10 Rounds. All creatures and unattended objects begin to lift off the ground at a rate of three metres per Round. At the end of this time, reality asserts itself and everything comes crashing down, likely dealing Damage to all those affected.' },
    { min: 91, max: 99, title: 'Daemonhost', text: 'The Psyker must immediately pass a Very Hard (–30) Willpower Test or be possessed by a Daemon and become a Daemonhost! Create an Unbound Daemonhost (See Chapter XII: Aliens, Heretics & Antagonists) that will immediately attack. Only the destruction of the Daemonhost will free the Psyker (though he may die anyway if his body is destroyed in the process). If the Psyker does somehow manage to survive this result, he automatically gains 4d10 Corruption Points.' },
    { min: 100, max: 100, title: 'Warp Feast', text: 'rift in reality is torn open and the Psyker is sucked into the warp with a little burping noise. He is no more.' },
];