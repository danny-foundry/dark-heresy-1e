export const ammo = [
    {
        id: 'standard',
        name: 'Standard',
    },
    {
        id: 'dumdum',
        name: 'Dumdum',
        callable: {
            preRoll: (attack) => attack.notices.push({ text: 'Armour is doubled against this attack' }),
            preDamage: (attack) => attack.damage.modifier += 2,
        }
    },
    {
        id: 'hotshot',
        name: 'Hotshot',
        callable: {
            preDamage: (attack) => {
                // +1 damage
                attack.damage.modifier += 1;

                // tearing
                attack.damage.dieCount ++;
                attack.damage.dropLowest ++;

                // penetration 4
                attack.penetrationModifier += 4;

                // clip size 1
                attack.weapon.update({
                    system: {
                        current_ammo: 0,
                    }
                });
            }
        }
    },
    {
        id: 'inferno',
        name: 'Inferno',
        callable: {
            preRoll: (attack) => attack.notices.push({ text: 'Victim must make an agility test to avoid catching fire.' }),
        }
    },
    {
        id: 'manstopper',
        name: 'Manstopper',
        callable: {
            preDamage: (attack) => attack.penetrationModifier += 3,
        }
    }
];

export function getAmmoById(id)
{
    let model = ammo.find((model) => model.id === id);
    if (!model) {
        return ammo[0];
    }
    return model;
}