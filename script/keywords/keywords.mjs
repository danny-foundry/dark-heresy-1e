export const keywords = [
    {
        id: 'unbalanced',
        callable: {
            preParry: (parry) => parry.rollModifiers.push({ label: 'Unbalanced Weapon', value: -10 })
        }
    },
    {
        id: 'balanced',
        callable: {
            preParry: (parry) => parry.rollModifiers.push({ label: 'Balanced Weapon', value: 10 })
        }
    },
    {
        id: 'unwieldy',
        callable: {
            preParry: (parry) => parry.rollModifiers.push({ label: 'Unwieldy, cannot parry', value: -1000 })
        }
    },
    {
        id: 'power_field',
        callable: {
            postParry: (parry) => {
                if (parry.success) {
                    parry.notices.push({
                        text: 'If the attacking weapon is NOT a power weapon, there is a 75% chance it is destroyed.'
                    })
                }
            }
        }
    },
    {
        id: 'unreliable',
        callable: {
            preRoll: (attack) => attack.jamThreshold = 91,
        }
    },
    {
        id: 'reliable',
        callable: {
            async postRoll(attack) {
                if (attack.ammo?.id === 'hotshot') {
                    attack.notices.push({
                        text: 'The weapon is no longer reliable due to it\'s overcharged powercell',
                    })
                    return;
                }

                if (!attack.isJam) {
                    return;
                }

                const data = new Roll("1d10");
                data.evaluate({ async: true });

                if (data.total !== 10) {
                    attack.isJam = false;
                    attack.isMiss = true;
                    attack.notices.push({
                        text: 'The weapons reliability saved it from jamming',
                    })
                }
            }
        }
    },
    {
        id: 'defensive',
        callable: {
            preRoll: (attack) => attack.targetModifiers.push({ label: 'Defensive weapon', value: -10 }),
        }
    },
    {
        id: 'overheats',
        callable: {
            preRoll: (attack) => attack.overheatThreshold = 91,
        }
    },
    {
        id: 'primitive',
        callable: {
            preDamage: (attack) => attack.damageNotices.push({ text: 'Armour points are doubled (unless the armour is primitive).' }),
        }
    },
    {
        id: 'recharge',
        callable: {
            preRoll: (attack) => attack.notices.push({ text: `${attack.weapon.name} requires a round to recharge.` }),
        }
    },
    {
        id: 'flame',
        callable: {
            preRoll: (attack) => attack.isTemplate = true,
        }
    },
    {
        id: 'unstable',
        callable: {
            postDamage: async (attack) => {
                let rollData = new Roll("1d10");
                await rollData.evaluate({ async: true });

                attack.damageNotices.push({ text: 'Unstable weapon: ' + rollData.total });
                if (rollData.total === 1) {
                    attack.damageNotices.push({ text: 'Attack fizzles, half damage' });
                    attack.damageValues = attack.damageValues.map((damage) => {
                        damage.damage /= 2;
                        return damage;
                    })
                } else if (rollData.total === 10) {
                    attack.damageNotices.push({ text: 'Attack is super effective, double damage' });
                    attack.damageValues = attack.damageValues.map((damage) => {
                        damage.damage *= 2;
                        return damage;
                    })
                }
            },
        }
    },
    {
        id: 'smoke',
        callable: {
            preRoll: async (attack) => {
                let rollData = new Roll("3d10");
                await rollData.evaluate({ async: true });
                const diameter = rollData.total;

                rollData = new Roll("2d10");
                await rollData.evaluate({ async: true });
                const duration = rollData.total;

                attack.isSmoke = true;
                attack.damageNotices.push({ text: `${attack.weapon.name} creates a ${diameter}m diameter smoke cloud for ${duration} rounds.` });
            }
        }
    },
    {
        id: 'shocking',
        callable: {
            preRoll: (attack) => attack.damageNotices.push({
                text: 'Any target that takes at least 1 damage after soak, must make a toughness test with +10 per point of armour on the affected location. On failure, they are stunned for a number of rounds equal to half the damage taken.'
            })
        }
    },
    {
        id: 'snare',
        callable: {
            preRoll: (attack) => attack.notices.push({ text: 'Any character hit by this weapon must make an Agility test or be immobilised (Strength or Agility test to escape).' }),
        }
    },
    {
        id: 'tearing',
        callable: {
            preDamage: (attack) => {
                attack.damage.dieCount ++;
                attack.damage.dropLowest ++;
            }
        }
    },
    {
        id: 'toxic',
        callable: {
            preRoll: (attack) => {
                attack.damageNotices.push({
                    text: 'Anyone who takes damage after soak, must make a Toughness test (-5 per point of damage) or take another 1d10 impact damage with no soak.',
                });
            }
        }
    },
    {
        id: 'scatter',
        callable: {
            // Handled by modifier
        }
    },
    {
        id: 'flexible',
        callable: {
            preRoll: (attack) => attack.notices.push({ text: 'This attack cannot be parried.' }),
        }
    },
    {
        id: 'inaccurate',
        callable: {
            // handled by Aim modifiers
        }
    },
    {
        id: 'accurate',
        callable: {
            // handled by Aim modifiers
        }
    }
];