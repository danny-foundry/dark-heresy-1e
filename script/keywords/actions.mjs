export const actions = [
    {
        id: "standard",
        name: "Standard",
        callable: {
            available: (actor, weapon) => true,
            disabled: (actor, weapon) => false,
        },
    },
    {
        id: "all-out",
        name: "All Out Attack (+20)",
        callable: {
            available: (actor, weapon) => weapon.type === 'melee_weapon',
            disabled: (actor, weapon) => false,
            preRoll: (attack) => attack.targetModifiers.push({ label: 'All out attack', value: 20 }),
        },
    },
    {
        id: "charge",
        name: "Charge (+10)",
        callable: {
            available: (actor, weapon) => weapon.type === 'melee_weapon',
            disabled: (actor, weapon) => false,
            preRoll: (attack) => attack.targetModifiers.push({ label: 'Charge!', value: 10 }),
        }
    },
    {
        id: "guarded",
        name: "Guarded (-10)",
        callable: {
            available: (actor, weapon) => weapon.type === 'melee_weapon',
            disabled: (actor, weapon) => false,
            preRoll: (attack) => attack.targetModifiers.push({ label: 'Guarded attack', value: -10 }),
        }
    },
    {
        id: "semi-auto",
        name: "Semi-auto (+10)",
        callable: {
            available: (actor, weapon) => weapon.type === 'ranged_weapon',
            disabled: (actor, weapon) => !weapon.model.hasSemiAuto,
            preRoll: (attack) => {
                attack.ammoExpended = attack.weapon.model.ammoUsageByFireMode.semi;
                attack.jamThreshold = 94;
                attack.targetModifiers.push({ label: 'Semi-auto', value: +10 });
            },
            postRoll: (attack) => {
                if (attack.result <= attack.target) {
                    let extraHits = Math.floor(attack.degree / 2);
                    extraHits = Math.min(attack.ammoExpended, extraHits);

                    attack.hitCount += extraHits;
                    attack.notices.push({ text: `Semi auto results in ${extraHits} extra hits.` });
                }
            },
        }
    },
    {
        id: "full-auto",
        name: "Full-auto (+20)",
        callable: {
            available: (actor, weapon) => weapon.type === 'ranged_weapon',
            disabled: (actor, weapon) => !weapon.model.hasFullAuto,
            preRoll: (attack) => {
                attack.ammoExpended = attack.weapon.model.ammoUsageByFireMode.full;
                attack.jamThreshold = 94;
                attack.targetModifiers.push({ label: 'Full-auto', value: +20 });
            },
            postRoll: (attack) => {
                if (attack.margin >= 0) {
                    let extraHits = Math.floor(attack.degree);
                    extraHits = Math.min(attack.ammoExpended, extraHits);

                    attack.hitCount += extraHits;
                    attack.notices.push({ text: `Full auto results in ${extraHits} extra hits.` });
                }
            },
        }
    },
    {
        id: "suppression",
        name: "Suppressive Fire (-20)",
        callable: {
            available: (actor, weapon) => weapon.type === 'ranged_weapon',
            disabled: (actor, weapon) => !weapon.model.hasFullAuto,
            preRoll: (attack) => {
                attack.ammoExpended = attack.weapon.model.ammoUsageByFireMode.full;
                attack.jamThreshold = 94;
                attack.targetModifiers.push({ label: 'Suppressive fire', value: -20 });
            },
        }
    }
];